import {
	request
} from '@/common/request.js'

import configs from '@/common/configs'

const baseURL = configs.HOST_DOMAIN_HGM

// 账号密码登录
export const submitLogin = (data) => request({
	baseURL,
	method: 'POST',
	url: '/app/mobile/user/mobileLogin',
	data
})

// 发送验证码
export const sendSMSVerificationCode = (mobile) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/user/mobileSendSMSVerificationCode',
	data: {
		mobile
	}
})

// 验证码登录
export const mobileSmsLogin = (data) => request({
	baseURL,
	method: 'POST',
	url: '/app/mobile/user/mobileSmsLogin',
	data
})

// 验证码找回密码
export const mobileSendSMSVerificationCodeForModify = (mobile) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/user/mobileSendSMSVerificationCodeForModify',
	data: {
		mobile
	}
})

// 获取用户信息
export const getMobileCurrentUserInfo = () => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/user/getMobileCurrentUserInfo',
})

/**
 * 
 {
   "account": "",
   "id": 0,
   "password": "",
   "repeatPassword": "",
   "verificationCode": ""
 }
 */
// 移动端用户短信修改密码
export const mobileUserSMSUpdatePassword = (data) => request({
	baseURL,
	method: 'PUT',
	url: '/app/mobile/user/mobileUserSMSUpdatePassword',
	data
})

/**
 * 
 {
   "account": "",
   "id": 0,
   "oldPassword": "",
   "password": "",
   "repeatPassword": ""
 }
 */
// 移动端用户修改密码
export const mobileUserUpdatePassword = (data) => request({
	baseURL,
	method: 'PUT',
	url: '/app/mobile/user/mobileUserUpdatePassword',
	data
})

// 退出登录
export const mobileSignOut = (account) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/user/mobileSignOut',
	data: {
		account
	}
})