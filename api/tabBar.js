import {
	request
} from '@/common/request.js'

import configs from '@/common/configs'
// 蔡小刚
const baseURLCXG = configs.HOST_DOMAIN_CXG
// 侯国民
const baseURLHGM = configs.HOST_DOMAIN_HGM
// 测试服务器
const baseURL = configs.HOST_DOMAIN_DEV
// overview 模块接口
// 蜀道所有分站列表
export const stationList = () => request({
	baseURL: baseURLCXG,
	method: 'GET',
	url: '/dashboard/mobile/digital/station/list',
})
// 查询蜀道所有信息
export const stationInfo = () => request({
	baseURL: baseURLCXG,
	method: 'GET',
	url: '/dashboard/mobile/station/info',
})

// 蜀道所有统计告警
export const incidentCount = () => request({
	baseURL: baseURLCXG,
	method: 'GET',
	url: '/dashboard/mobile/incident/count',
})
// 查询蜀道所有逆变器状态
export const stationCount = () => request({
	baseURL: baseURLCXG,
	method: 'GET',
	url: '/dashboard/mobile/station/count',
})
// 获取天气预报
export const queryMeteorological = () => request({
	baseURL: baseURLCXG,
	method: 'POST',
	url: '/app/mobile/meteorological/queryMeteorological',
})

// 获取单个分站信息
export const stationGet = (data) => request({
	baseURL: baseURLCXG,
	method: 'GET',
	url: '/dashboard/mobile/station/get/'+data,
})

// 某个电站的发电量统计
export const digitalTrend = (stationId, data) => request({
	baseURL: baseURLCXG,
	method: 'POST',
	url: '/dashboard/mobile/digital/trend/'+ stationId,
	data
})

// 逆变器列表
export const invertList = (stationId) => request({
	baseURL: baseURLCXG,
	method: 'POST',
	url: '/dashboard/mobile/digital/invert/list/'+ stationId
})

// 
// 分页获取告警列表
export const incidentMessageInfoPageList = (data) => request({
	baseURL: baseURLCXG,
	method: 'POST',
	url: '/app/mobile/incidentMessageInfo/pageList',
	data
})

// 获取各个告警数量
export const getIncidentMsgLevelCount = (data) => request({
	baseURL: baseURLCXG,
	method: 'POST',
	url: '/app/mobile/incidentMessageInfo/getIncidentMsgLevelCount',
	data
})

// 恢复告警 信息
export const incidentMessageInfoResume = (data) => request({
	baseURL: baseURLCXG,
	method: 'GET',
	url: '/app/mobile/incidentMessageInfo/resume',
	data
})

// 查询告警 信息详情
export const incidentMessageInfoGetById = (data) => request({
	baseURL: baseURLCXG,
	method: 'GET',
	url: '/app/mobile/incidentMessageInfo/getById',
	data
})

// 获取 用户 下拉列表
export const workOrderInfoUserList = (data) => request({
	baseURL: baseURLCXG,
	method: 'GET',
	url: '/app/mobile/workOrderInfo/userList',
	data
})
// 侯国民
// 创建工单
export const incidentMessageInfoCreateWork = (data) => request({
	baseURL: baseURLHGM,
	method: 'POST',
	url: '/app/mobile/incidentMessageInfo/createWork',
	data
})

// 分页获取缺陷信息
export const defectRecordInfoPageList = (data) => request({
	baseURL: baseURLHGM,
	method: 'POST',
	url: '/app/mobile/defectRecordInfo/pageList',
	data
})

// 获取当前各个缺陷等级数量
export const defectRecordInfoGetDefectRecordLevelCount = (data) => request({
	baseURL: baseURLHGM,
	method: 'GET',
	url: '/app/mobile/defectRecordInfo/getDefectRecordLevelCount',
	data
})

// 缺陷记录 根据缺陷ID获取缺陷信息
export const defectRecordInfoGetById = (data) => request({
	baseURL: baseURLHGM,
	method: 'GET',
	url: '/app/mobile/defectRecordInfo/getById',
	data
})

// 缺陷记录 创建工单
export const defectRecordInfoCreateOrder = (data) => request({
	baseURL: baseURL,
	method: 'POST',
	url: '/app/mobile/defectRecordInfo/createOrder',
	data
})

// 获取分电站下拉选择
export const substationSubstationSelectList = (data) => request({
	baseURL: baseURL,
	method: 'GET',
	url: '/app/mobile/station/substation/mobileSubstationSelectList',
	data
})

// 上传文件
export const fileUploadFile = (data) => request({
	baseURL: baseURL,
	method: 'POST',
	url: '/app/mobile/file/uploadFile',
	data
})

// 缺陷记录 添加或更新
export const defectRecordInfoAddOrUpdate = (data) => request({
	baseURL: baseURL,
	method: 'POST',
	url: '/app/mobile/defectRecordInfo/addOrUpdate',
	data
})

// 逆变器发电量趋势
export const powerTrend = (stationId, data) => request({
	baseURL: baseURL,
	method: 'POST',
	url: '/dashboard/mobile/invert/power/trend/'+stationId,
	data
})

// 
// 获取分电站下拉选择
export const productionDeviceDobileGetInverterInfo = (data) => request({
	baseURL: baseURL,
	method: 'GET',
	url: '/device/mobile/production-device/mobileGetInverterInfo',
	data
})

export const getProductionHasBindingList = (data) => {
  return request({
	  baseURL: baseURLHGM,
    url: `/device/mobile/production-device/mobileGetProductionHasBindingList`,
    method: 'POST',
    data
  })
}

export const getActiveRecordNumInfo = () => {
  return request({
	  baseURL: baseURLHGM,
    url: `/app/mobile/incidentMessageInfo/getActiveRecordNumInfo`,
    method: 'GET'
  })
}