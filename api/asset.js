import {
	request
} from '@/common/request.js'

import configs from '@/common/configs'

const baseURL = configs.HOST_DOMAIN_HGM

/**
 * 投产设备-获取已绑定资产的投产设备下拉列表（未分类）
 */
export const getProductionHasBindingListNotGroup = (data) => request({
	baseURL,
	method: 'POST',
	url: '/device/mobile/production-device/mobileGetProductionHasBindingListNotGroup',
	data
})

/**
 * 扫二维码获取资产设备信息
 * 
 * @param { object } params
 * @param { string } params.assetId
 */
export const getMobileAssetQRCodeRequest = (params) => request({
	method: 'GET',
	url: '/device/mobile/asset/mobileAssetQRCodeRequest',
	params
})