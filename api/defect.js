import {
	request
} from '@/common/request.js'

import configs from '@/common/configs'

const baseURL = configs.HOST_DOMAIN_HGM

/**
 * [新增|编辑]缺陷
 */
export const saveDefect = (data) => request({
	baseURL,
	method: 'POST',
	url: `/app/mobile/patrolInspection/addOrUpdateDefectRecord`,
	data
})

/**
 * 删除缺陷
 */
export const delDefect = (data) => request({
	baseURL,
	method: 'POST',
	url: `/app/mobile/patrolInspection/deleteDefectRecord`,
	data
})

/**
 * 获取缺陷详情
 */
export const getDefect = (params) => request({
	baseURL,
	method: 'GET',
	url: `/app/mobile/defectRecordInfo/getById`,
	data: params
})

/**
 * 获取缺陷类型列表
 */
export const getDefectTypesList = () => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/patrolInspection/getPatrolInspectionTypes',
})