import {
	request
} from '@/common/request.js'
import configs from '@/common/configs'

const baseURL = configs.HOST_DOMAIN_HGM
// 分页获取我的工单列表
export const myOrderList = (data) => {
	return request({
		baseURL,
		url: `/app/mobile/workOrderInfo/myOrderList`,
		method: 'post',
		data
	})
}
// 分页获取工单列表
export const workOrderInfoPageList = (data) => {
	return request({
		baseURL,
		url: `/app/mobile/workOrderInfo/pageList`,
		method: 'post',
		data
	})
}
// 手动添加工单
export const workOrderAdd = (data) => {
	return request({
		baseURL,
		url: `/app/mobile/workOrderInfo/add`,
		method: 'post',
		data
	})
}
// 删除工单
export const workOrderDelete = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/workOrderInfo/delete',
	params
})

// 用户下拉
export const userList = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/workOrderInfo/userList',
	params
})

// 工单详情
export const getById = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/workOrderInfo/getById',
	params
})
// 设备下拉
export const getProduction = (data) => {
	return request({
		baseURL,
		url: '/device/mobile/production-device/mobileGetProductionHasBindingList',
		method: 'post',
		data
	})
}
// 关闭工单
export const closeWorkOrderById = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/workOrderInfo/closeWorkOrderById',
	params
})
//工单花费时间
export const calculateWorkOrderProcessTakeTimeById = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/workOrderInfo/calculateWorkOrderProcessTakeTimeById',
	params
})
//开始处理工单
export const beginWorkOrder = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/workOrderInfo/beginWorkOrder',
	params
})
//挂起工单
export const pendingWorkOrder = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/workOrderInfo/pendingWorkOrder',
	params
})
// 编辑工单
export const update = (data) => {
	return request({
		baseURL,
		url: '/app/mobile/workOrderInfo/update',
		method: 'post',
		data
	})
}
// 获取资产模板下拉
export const mobileGetAssetModelSelectList = (params) => request({
	baseURL,
	method: 'GET',
	url: '/device/mobile/asset/asset-model/mobileGetAssetModelSelectList',
	params
})
// 获取资产设备下拉
export const mobileGetAssetSelectList = (data) => {
	return request({
		baseURL,
		url: '/device/mobile/asset/info/mobileGetAssetSelectList',
		method: 'post',
		data
	})
}
// 获取资产设备下拉
export const mobileGetAssetNotOutputSelectList = (data) => {
	return request({
		baseURL,
		url: '/device/mobile/asset/info/mobileGetAssetNotOutputSelectList',
		method: 'post',
		data
	})
}
// 检修处理
export const updateWorkOrderProcess = (data) => {
	return request({
		baseURL,
		url: '/app/mobile/workOrderInfo/updateWorkOrderProcess',
		method: 'post',
		data
	})
}

// 移动端根据当前用户获取其下所有电站、分电站
export const mobileGetAllStationByCurrent = () => {
	return request({
		baseURL,
		url: '/app/mobile/station/mobileGetAllStationByCurrent',
		method: 'get'
	})
}

// 移动端投产设备-获取定位设备数据集合
export const mobileGetDevicePositioningSelectList = (data) => {
	return request({
		baseURL,
		url: '/device/mobile/production-device/mobileGetDevicePositioningSelectList',
		method: 'post',
		data
	})
}
// 移动端投产设备-更新设备的定位信息
export const mobileDevicePositioningModify = (data) => {
	return request({
		baseURL,
		url: '/device/mobile/production-device/mobileDevicePositioningModify',
		method: 'put',
		data
	})
}

// 上传文件
export const uploadFile = (data) => {
  return request({
	  baseURL: baseURLHGM,
    url: `/app/mobile/file/uploadFile`,
    method: 'POST',
    data
  })
}

// mobileGetStationBasicInfo
export const mobileGetStationBasicInfo = (data) => {
	return request({
		baseURL,
		url: '/app/mobile/station/mobileGetStationBasicInfo',
		method: 'GET',
		data
	})
}