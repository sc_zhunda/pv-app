import {
	request
} from '@/common/request.js'
import configs from '@/common/configs'

const baseURL = configs.HOST_DOMAIN_HGM
// 分页获取检修计划列表
export const getPlanList = (data) => {
	return request({
		baseURL,
		url: `/app/mobile/overhaul-plan/pageList`,
		method: 'post',
		data
	})
}
// 创建检修计划
export const creatPlan = (data) => {
	return request({
		baseURL,
		url: `/app/mobile/overhaul-plan/createOverhaulPlan`,
		method: 'post',
		data
	})
}
// 生成检修计划配置
export const creatPlanConfig = (data) => {
	return request({
		baseURL,
		url: `/app/mobile/overhaul-plan/createStaffing`,
		method: 'post',
		data
	})
}
// 获取定检修计划详情
export const getPlanDetail = (planId) => {
	return request({
		baseURL,
		url: `/app/mobile/overhaul-plan/${planId}`,
		method: 'get',
	})
}
// 删除检修计划
export const delPlan = (planId) => {
	return request({
		baseURL,
		url: `/app/mobile/overhaul-plan/${planId}`,
		method: 'DELETE',
	})
}
// 获取工单创建人下拉
export const getUserList = () => {
	return request({
		baseURL,
		url: `/app/mobile/workOrderInfo/userList`,
		method: 'get',
	})
}
// 获取工单列表
export const getOrderList = (data) => {
	return request({
		baseURL,
		url: `/app/mobile/workOrderInfo/getWorkOrderByIds`,
		method: 'post',
		data
	})
}