import {
	request
} from '@/common/request.js'

import configs from '@/common/configs'

const baseURL = configs.HOST_DOMAIN_HGM

/**
 * 获取分页列表
 */
export const getInspectionList = (data) => request({
	baseURL,
	method: 'POST',
	url: '/app/mobile/patrolInspection/queryPage',
	data
})

/**
 * 获取巡检任务详情
 */
export const getInspection = (id) => request({
	baseURL,
	method: 'GET',
	url: `/app/mobile/patrolInspection/detail/${id}`,
})

/**
 * 添加
 */
export const createInspection = (data) => request({
	baseURL,
	method: 'POST',
	url: '/app/mobile/patrolInspection/insert',
	data
})

/**
 * 编辑
 */
export const updateInspection = (data) => request({
	baseURL,
	method: 'POST',
	url: '/app/mobile/patrolInspection/update',
	data
})

/**
 * 开始巡检
 */
export const updateInspectionStart = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/patrolInspection/beginPatrolInspection',
	params
})

/**
 * 巡检完成
 */
export const updateInspectionComplete = (params) => request({
	baseURL,
	method: 'GET',
	url: '/app/mobile/patrolInspection/completePatrolInspection',
	params
})

/**
 * 获取分站列表
 */
export const getSubStationList = () => request({
	baseURL,
	method: 'POST',
	url: '/app/mobile/patrolInspection/substationList',
})