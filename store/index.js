import Vue from 'vue'
import Vuex from 'vuex'
import common from './module/common';

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		common
	},
	state: {},
	getters: {},
	mutations: {},
	actions: {}
})

export default store