import {
	set
} from 'lodash';

/**
 * 更新state的普通属性（支持链式更新）
 *
 * @param { state } state
 * @param { { keyname: string; data: any } | { keyname: string; data: any }[] } payload 支持批量更新属性
 */
export const updateState = (state, payload) => {
	if (!payload) return;

	if (typeof payload === 'object' && Object.prototype.hasOwnProperty.call(payload, 'keyname')) {
		// 转换为数组统一处理
		payload = [payload];
	}

	payload.forEach(({
		keyname,
		data
	}) => {
		set(state, keyname, data);
	});
};