import {
	updateState
} from '../utils.js'

export default {
	namespaced: true,

	state: {
		token: uni.getStorageSync('token') || '',
		userInfo:JSON.parse(uni.getStorageSync('userInfo') || '{}') || {}
	},

	getters: {},

	mutations: {
		/**
		 * 更新state的普通属性
		 *
		 * @param { state } state
		 * @param { { keyname: string; data: any } | { keyname: string; data: any }[] } payload 支持批量更新属性
		 */
		updateState,
	},

	actions: {}
}