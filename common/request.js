/**
 * @param { object } payload
 * @param { string } payload.url
 * @param { 'GET' | 'POST' | 'PUT' | 'DELETE' } [payload.method]
 * @param { string } [payload.baseURL]
 * @param { object } [payload.params]
 * @param { object } [payload.data]
 * @param { object } [payload.header]
 */
export const request = (payload) => new Promise((resolve, reject) => {
	if (payload.params) payload.data = payload.params
	if (payload.baseURL) payload.url = payload.baseURL + payload.url

	const options = {
		...payload,
		success: res => {
			if ([200, 201, 204].includes(res.statusCode)) {
				resolve(res.data)
				return;
			}
			reject(res)
		},
		fail: err => reject
	}

	uni.request(options)
})