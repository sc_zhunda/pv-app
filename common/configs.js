const {
	PROXY_IP,
	PROXY_IP_DEV,
	PROXY_IP_HGM,
	PROXY_IP_CXG
} = require('@/common/config-proxy.js')

// H5环境（配合代理）
// #ifdef WEB

const HOST_DOMAIN = '';

// 测试环境
const HOST_DOMAIN_DEV = process.env.NODE_ENV === 'development' ? '/test' : HOST_DOMAIN;

// 侯国明
const HOST_DOMAIN_HGM = process.env.NODE_ENV === 'development' ? '/hgm' : HOST_DOMAIN;

// 蔡小刚
const HOST_DOMAIN_CXG = process.env.NODE_ENV === 'development' ? '/cxg' : HOST_DOMAIN;

// #endif

// 非H5环境
// #ifndef WEB

const HOST_DOMAIN = PROXY_IP;

// 测试环境
const HOST_DOMAIN_DEV = process.env.NODE_ENV === 'development' ? PROXY_IP_DEV : HOST_DOMAIN;

// 侯国明
const HOST_DOMAIN_HGM = process.env.NODE_ENV === 'development' ? PROXY_IP_HGM : HOST_DOMAIN;

// 蔡小刚
const HOST_DOMAIN_CXG = process.env.NODE_ENV === 'development' ? PROXY_IP_CXG : HOST_DOMAIN;

// #endif

export default {
	// 服务器域名
	// HOST_DOMAIN: process.env.NODE_ENV === 'development' ? 'http://192.168.6.2:29380' : 'http://192.168.6.2:29380',

	// 服务器域名baseURL
	// HOST_DOMAIN,
	HOST_DOMAIN_DEV,
	HOST_DOMAIN_HGM, // 侯国明
	HOST_DOMAIN_CXG, // 蔡小刚

	// 静态资源路径
	MEDIA_BASE_URL: PROXY_IP
	// MEDIA_BASE_URL: 'http://192.168.3.100:3032'
}