/**
 * 获取默认状态
 *
 * @param scene 场景如：role, user, order
 *
 * @todo 根据scene返回对应状态options
 * @todo 不传则返回全部options
 */
export const GetStateOptionsAll = () => ({
	// 资产类型
	'asset-type': [
		// 标签：NBQ:逆变器，SYZ:升压站，GFB:光伏板，XB:箱变，HLX:汇流箱
		{
			label: '逆变器',
			value: 'NBQ',
			type: 'primary'
		},
		{
			label: '升压站',
			value: 'SYZ',
			type: 'primary'
		},
		{
			label: '光伏板',
			value: 'GFB',
			type: 'primary'	
		},
		{
			label: '箱变',
			value: 'XB',
			type: 'primary'
		},
		{
			label: '汇流箱',
			value: 'HLX',
			type: 'primary'
		}
	],
	// 工单
	workorder: [{
			label: '告警',
			value: 'ALARM',
			type: 'danger'
		},
		{
			label: '人工',
			value: 'ARTIFICIAL',
			type: 'artificial'
		},
		{
			label: '缺陷',
			value: 'DEFECT_RECORD',
			type: 'defect'
		},
		{
			label: '待处理',
			value: '0',
			type: 'pending'
		},
		{
			label: '处理中',
			value: '1',
			type: 'processing'
		},
		{
			label: '已处理',
			value: '2',
			type: 'processed'
		},
		{
			label: '已关闭',
			value: '3',
			type: 'closed'
		},
		{
			label: '挂起',
			value: '-1',
			type: 'Suspend'
		},
	],

	// 巡检 - 类型
	'inspection-type': [{
			label: '日常巡检',
			value: 'NORMAL_PATROL_INSPECTION',
			type: 'ins-success'
		},
		{
			label: '消缺',
			value: 'CONFIRMATION_OF_DEFECT_ELIMINATION',
			type: 'ins-danger'
		},
		{
			label: '消警',
			value: 'ALARM_ELIMINATION_CONFIRM',
			type: 'ins-danger'
		}
	],
	// 设备
	'equipment': [{
		label: '正常',
		value: '1',
		type: 'success'
	}, {
		label: '离线',
		value: '2',
		type: 'info'
	}, {
		label: '故障',
		value: '3',
		type: 'danger'
	}],
// 缺陷 - 工单状态
'workOrderStatus': [{
	label: '待创建',
	value: '0',
	type: 'success'
},{
	label: '已创建',
	value: '1',
	type: 'success'
},{
	label: '处理中',
	value: '2',
	type: 'success'
},{
	label: '已完成',
	value: '3',
	type: 'success'
}],
	// 告警
	'alarm': [{
		label: '已恢复',
		value: '1',
		type: 'success'
	}, {
		label: '未恢复',
		value: '0',
		type: 'info'
	}, {
		label: '已恢复(手动恢复)',
		value: '2',
		type: 'success'
	},],

	// 巡检 - 状态
	'inspection-status': [
		// 0未开始 1处理中 2已完成
		{
			label: '待执行',
			value: '0',
			type: 'warning'
		},
		{
			label: '执行中',
			value: '1',
			type: 'success'
		},
		{
			label: '已执行',
			value: '2',
			type: 'info'
		}
	],
	// 缺陷 - 类型
	'defect-type': [{
			label: '外观破损',
			value: 'VISIBLY_DAMAGED',
			type: 'warning'
		},
		{
			label: '设备故障',
			value: 'DEVICE_FAULT',
			type: 'success'
		},
		{
			label: '数值异常',
			value: 'ABNORMAL_DATA',
			type: 'info'
		},
		{
			label: '来自告警',
			value: 'ALARM',
			type: 'info'
		},
		{
			label: '其他',
			value: 'UNKNOW',
			type: 'info'
		}
	],
	// 告警 - 等级
	'alert-level': [{
			label: '提示',
			value: 'PROMPT',
			type: 'alert-info'
		},
		{
			label: '普通',
			value: 'GENERAL_ALARM',
			type: 'alert-primary'
		},
		{
			label: '严重',
			value: 'SERIOUS_ALARM',
			type: 'alert-warning'
		},
		{
			label: '紧急',
			value: 'EMERGENCY_ALARM',
			type: 'alert-danger'
		}
	]
});

// 获取状态
export const GetStateOptions = (scene) => {
	const stateOptsAll = GetStateOptionsAll();
	if (typeof stateOptsAll[scene] === 'undefined') {
		console.error(`GetStateOptions：未设置${scene}场景值`);
		return [];
	}
	return stateOptsAll[scene];
};

/**
 * 根据场景、状态值，获取描述
 *
 * 注：建议直接使用标签<PipeStateTags type="" state="" />
 *
 * @param scene 场景
 * @param value 状态值
 * @returns
 */
export const GetLabelByStateOptions = (
	scene,
	value
) => {
	const stateData = GetStateOptions(scene);
	const itemData = stateData.find((item) => item.value === value);
	return itemData?.label || '';
};