// 9580 - 生产环境
// 9480 - 测试环境
// 29380 - 开发环境

// 代理IP地址
// const PROXY_IP = 'http://125.71.203.166:9580/solar-cloud-monitoring'; // 生产环境
// const PROXY_IP = 'http://125.71.203.166:9480/solar-cloud-monitoring'; // 测试环境
const PROXY_IP = 'http://192.168.253.3:29380/solar-cloud-monitoring'; // 开发环境
// const PROXY_IP = process.env.NODE_ENV === 'development' ? 'http://125.71.203.166:9480/solar-cloud-monitoring' :
// 	'http://125.71.203.166:9580/solar-cloud-monitoring'

// 测试环境
// const PROXY_IP_DEV = 'http://192.168.253.3:9480/solar-cloud-monitoring';
// const PROXY_IP_DEV = 'http://192.168.253.3:9480/solar-cloud-monitoring';
const PROXY_IP_DEV = PROXY_IP;

// 开发环境
// const PROXY_IP_DEV = 'http://192.168.253.3:9480/solar-cloud-monitoring';

// 侯国明
// const PROXY_IP_HGM = 'http://192.168.31.64:29380/solar-cloud-monitoring';
// const PROXY_IP_HGM = 'http://192.168.43.245:29380/solar-cloud-monitoring';
// const PROXY_IP_HGM = 'http://192.168.43.114:29480/solar-cloud-monitoring';
const PROXY_IP_HGM = PROXY_IP_DEV;

// 蔡小刚
// const PROXY_IP_CXG = 'http://192.168.31.18:8082';
const PROXY_IP_CXG = PROXY_IP_DEV;

// 代理IP（不建议调用）
exports.PROXY_IP = PROXY_IP;
exports.PROXY_IP_DEV = PROXY_IP_DEV; // 测试环境
exports.PROXY_IP_HGM = PROXY_IP_HGM; // 侯国明
exports.PROXY_IP_CXG = PROXY_IP_CXG; // 蔡小刚