"use strict";

const configs = require('./common/config-proxy.js')

module.exports = {
	devServer: {
		proxy: {
			// 测试服务器
			'/test': {
				target: configs.PROXY_IP_DEV,
				pathRewrite: {
					'^/test': ''
				}
			},
			// 侯国明
			'/hgm': {
				target: configs.PROXY_IP_HGM,
				pathRewrite: {
					'^/hgm': ''
				}
			},
			// 蔡小刚
			'/cxg': {
				target: configs.PROXY_IP_CXG,
				pathRewrite: {
					'^/cxg': ''
				}
			}
		},
	}
}